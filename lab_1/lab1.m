function lab1()
    %fin = fopen("X.txt", "r");
    %X = fscanf(fin, "%f,");
    X = [-2.54,-0.79,-4.27,-3.09,-3.82,-0.61,-0.64,-1.24,-1.73,-2.91,-1.48,-1.28,-0.37,-1.88,-2.19,-1.61,-1.52,-3.17,-1.36,-3.08,-3.11,-3.07,-1.57,-1.51,-2.37,-0.58,-3.05,-2.93,-1.01,-1.40,-2.06,-3.05,-1.84,-1.24,-1.89,-2.06,-1.59,-2.83,-1.07,-2.96,-3.17,-3.08,-0.49,-3.11,-3.14,-2.30,-3.99,-1.56,-1.28,-3.46,-2.63,-0.82,-2.18,-0.89,-3.08,-1.13,-1.62,-1.06,-2.98,-1.55,-1.49,-1.65,-1.45,-2.29,-0.85,-1.44,-2.87,-2.40,-2.13,-3.52,-1.42,-3.64,-3.47,-2.05,-2.39,-2.07,-0.80,-1.52,-3.92,-2.22,-0.78,-2.60,-1.78,-1.61,-1.65,-2.06,-3.33,-3.41,-1.97,-1.74,-2.04,0.01,-1.37,-3.15,-2.35,-3.66,-1.79,-2.56,-1.87,-1.06,-0.64,-2.49,-1.85,-1.40,-0.86,-0.17,-0.62,-2.85,-2.12,-1.17,-2.48,-1.65,-3.74,-2.87,-3.15,-1.89,-1.34,-4.33,-0.96,-1.79];
    xmin = min(X);
    xmax = max(X);
    R = xmax - xmin;

    len = length(X);
    MX = sum(X) / len; %or use mean(X)

    s = 0;
    for i = 1:len
        s = s + (X(i) - MX)^2;
    end

    DX = s / (len - 1);

    fprintf("xmin = %f\n", xmin);
    fprintf("xmax = %f\n", xmax);
    fprintf("R = %f\n", R);
    fprintf("MX = %f\n", MX);
    fprintf("DX = %f\n", DX);

    m = ceil(log2(len) + 2);

    [x, y] = intervals(sort(X), m);
    stairs(x, y), grid;
    hold on
    x = xmin: R/(50) :xmax;
    plot(x, normpdf(x, MX, sqrt(DX)));

    figure;

    plot(x, normcdf(x, MX, sqrt(DX)));
    [y, x] = ecdf(X);
    hold on
    stairs(x, y);
end

function [x_ranges, f_emp] = intervals(X, m)
    count = zeros(1, m+1);  
    delta = (X(end) - X(1)) / m;

    x_ranges = X(1):delta:X(end);

    j = 1;
    n = length(X);

    for i = 1:n
        if X(i) >= x_ranges(j+1)
            j = j + 1;
            fprintf('[%.2f;%.2f)\t', x_ranges(j-1), x_ranges(j));
        end
        count(j) = count(j) + 1;
    end
    fprintf('[%2.2f;%2.2f]\n', x_ranges(m), x_ranges(m + 1));

    f_emp = count(1:m+1);
    for i = 1:m+1
        f_emp(i) = count(i) / (n*delta); 
    end
    return
end